# react-redux starter skeleton

### Getting Started

There are two methods for getting started with this repo.

#### Familiar with Git?
Checkout this repo, install dependencies, then start the gulp process with the following:

```
> git clone https://bitbucket.org/silvamfilipe/react-starter.git
> cd react-starter
> npm install
> npm start
```

#### Not Familiar with Git?
Click [here](https://bitbucket.org/silvamfilipe/react-starter/downloads) then download the .zip file.  Extract the contents of the zip file, then open your terminal, change to the project directory, and:

```
> npm install
> npm start
```
