import React, { Component } from 'react';

export default class App extends Component {
  render() {
    return (
      <div className="container">
         <div className="starter-template">
            <h1>React-Redux starter skeleton with Bootstrap 4</h1>
            <p className="lead">Use this document as a way to quickly start any new project.<br/>All you get is this text and a mostly barebones HTML document.</p>
        </div>
      </div>
    );
  }
}
